// Task #1
// ============================================================================================
(function (window, undefined) {
  'use strict';

  // Still we do not use built-in parseInt(@arg, @base) funtion
  // I decided to make my own pow(@x, @n) function instead of Math.pow(@x, @n)
  function pow(x, n) {
    return n !== 1 ? x * pow(x, n - 1) : x;
  }

  function bytes2int(bytes) {
    if (!bytes || typeof bytes !== 'string' || !(/^[0,1]+$/).test(bytes)) {
      return typeof bytes === 'number' ? bytes : NaN;
    } else {
      var res = 0,
        bytesArr = bytes.split('').reverse();

      for (var i = 0, len = bytesArr.length; i < len; i++) {
        res += !!+bytesArr[i] && pow(2, i + 1) || 0;
      }

      return res / 2;
    }
  }

  function calculate() {
    var res = 0;

    Array.prototype.forEach.call(arguments, function (item) {
      res += bytes2int(item);
    });

    return arguments.length ? res : NaN;
  }

  // Translate to global scope
  window.calculate = calculate;
})(window);

// Task #2
// ============================================================================================
(function (window, undefined) {
  'use strict';

  var Calc = {};

  Calc['Number'] = function (val) {
    var that = this;

    this.val = val;

    return function (operation) {
      if (!operation) {
        return that.val;
      } else {
        return operation.operation(that.val, operation.num);
      }
    }
  }

  Calc['Operation'] = function (operation) {
    var that = this;

    that.operation = operation;

    return function (num) {
      return {
        operation: operation,
        num: num
      }
    };
  }

  var numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'],
    operations = [
      {
        name: 'times',
        fn: function (a, b) {
          return a * b
        }
      },
      {
        name: 'dividedBy',
        fn: function (a, b) {
          return a / b
        }
      },
      {
        name: 'plus',
        fn: function (a, b) {
          return a + b
        }
      },
      {
        name: 'minus',
        fn: function (a, b) {
          return a - b
        }
      }
];

  numbers.forEach(function (item, i) {
    window[item] = new Calc.Number(i);
  });

  operations.forEach(function (item) {
    window[item.name] = new Calc.Operation(item.fn);
  });
})(window);

// Task #3
// ============================================================================================
(function (window, undefined) {
  'use strict';

  function defaultArguments(fn, argsObj) {
    fn = fn || function () {};
    argsObj = argsObj || {};

    if (fn.argsObj) {
      for (var key in fn.argsObj) {
        if (!argsObj[key]) {
          argsObj[key] = fn.argsObj[key];
        }
      }
    }

    fn = fn.original || fn;

    // Returns array of fn arguments
    function getArgsNamesArr() {
      var fnStr = fn.toString(),
        argsNamesStr = (/\((.*)\)/).exec(fnStr)[1].replace(/\s/g, ''),
        argsNamesArr = argsNamesStr.split(',');

      return argsNamesArr;
    }

    // Array of fn arguments
    var argsNamesArr = getArgsNamesArr();

    // Returns array of default arguments
    function makeNewArgsArr() {
      return argsNamesArr.map(function (item) {
        return argsObj[item] || undefined;
      });
    }

    // Array of default arguments
    var newArgsArr = makeNewArgsArr();

    // New function that will be returned by defaultArguments function
    function newFn() {
      var originalArgsArr = Array.prototype.slice.call(arguments), // cast nf() arguments to Array
        updatedArgs = newArgsArr.map(function (arg, i) { // generate new args Array by mixing incoming original args and defaults
          return originalArgsArr[i] !== undefined ? originalArgsArr[i] : arg;
        });
      
      newFn.argsObj = argsObj;
      newFn.original = fn;

      return fn.apply(this, updatedArgs);
    };

    return newFn;
  }

  // Translate to global scope
  window.defaultArguments = defaultArguments;
})(window);

// Task #4
// ============================================================================================
(function (window, undefined) {
  'use strict';

  var schedules = [
    [['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
    [['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
    [['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
  ];

  function checkForMeetingAvailable(start, duration, attendees) {
    start = start || '09:00';
    duration = duration || 60;
    attendees = attendees || [0];

    var attendee;

    if (attendees.length == 1) {
      attendee = attendees[0];
    } else {
      var finalResult = true;

      attendees.forEach(function (attendee) {
        var isAvailable = checkForMeetingAvailable(start, duration, [attendee]);

        if (!isAvailable) {
          finalResult = null;
        }
      });

      return finalResult;
    }

    var result = true,
      attendeeSchedule = schedules[attendee],
      end = timeAddDuration(start, duration),
      warn = '';

    // Check preconditions
    if (start < '09:00' || end > '19:00') {
      warn = 'Out of work day hours';
      console.warn(warn);
      //throw new Error(warn);
      return null;
    }

    if (duration > 60 * 10) {
      warn = 'Longer than work day';
      console.warn(warn);
      //throw new Error(warn);
      return null;
    }

    for (var i = 0, len = attendeeSchedule.length; i < len; i++) {
      var meeting = attendeeSchedule[i],
        meetingStart = meeting[0],
        meetingEnd = meeting[1];

      var startWithOtherMeeting = start == meetingStart,
        endWithOtherMeeting = end == meetingEnd,
        endOnOtherMeeting = end > meetingStart && end < meetingEnd;

      if (
        startWithOtherMeeting ||
        endWithOtherMeeting ||
        endOnOtherMeeting
      ) {
        console.warn('Cross with other meeting');
        result = null;
      }
    }

    return result;

    // Helpers
    function timeAddDuration(start, duration) {
      var startArr = start.split(':'),
        startH = parseInt(startArr[0]),
        startM = parseInt(startArr[1]),
        durationM = duration % 60,
        durationH = (duration - durationM) / 60;

      var sumM = startM + durationM;

      if (sumM >= 60) {
        sumM = sumM % 60;
        durationH++;
      }

      var sumH = startH + durationH;

      function formatTime(val) {
        return val >= 10 ? val : '0' + val;
      }

      return formatTime(sumH) + ':' + formatTime(sumM);
    }
  }

  // Translate to global scope
  window.checkForMeetingAvailable = checkForMeetingAvailable;
})(window);