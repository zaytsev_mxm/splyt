(function () {
  'use strict';

  // Task #1
  // ============================================================================================
  describe("Task #1", function () {

    describe("Calculate sum of proper arguments", function () {

      it("Calculate sum of 2 arguments", function () {
        assert.equal(calculate('10', '10'), 4);
        assert.equal(calculate('10', '0'), 2);
        assert.equal(calculate('101', '10'), 7);
      });

      it("Calculate sum of N > 2 arguments", function () {
        assert.equal(calculate('101', '10', '01'), 8);
        assert.equal(calculate('10', '0', '01', '11'), 6);
        assert.equal(calculate('101', '10', '11', '10', '0'), 12);
      });

      it("Return Int value for single argument", function () {
        assert.equal(calculate('101'), 5);
        assert.equal(calculate('10'), 2);
        assert.equal(calculate('11'), 3);
      });

    });

    describe("Work normally for Int arguments", function () {

      it("Calculate sum of only Int arguments", function () {
        assert.equal(calculate(10, 10), 20);
        assert.equal(calculate(10, 0), 10);
        assert.equal(calculate(101, 10), 111);
      });

      it("Calculate sum of mixed Int and Bit arguments", function () {
        assert.equal(calculate(10, '10'), 12);
        assert.equal(calculate('10', 0), 2);
        assert.equal(calculate(101, '11', 10), 114);
      });

    });

    describe("Fail on non-valid arguments", function () {

      it("NaN for non-valid strings", function () {
        assert.isNaN(calculate('foo'));
        assert.isNaN(calculate('foo', 'bar'));
        assert.isNaN(calculate('148'));
        assert.isNaN(calculate('10', '14'));
      });

      it("NaN for no arguments passed", function () {
        assert.isNaN(calculate());
      });

    });
  });

  // Task #2
  // ============================================================================================
  describe("Task #2", function () {

    describe("Calculate basic sequences", function () {

      it("Operations with 2 operands", function () {
        assert.equal(seven(times(five())), 35);
        assert.equal(four(plus(nine())), 13);
        assert.equal(eight(minus(three())), 5);
        assert.equal(six(dividedBy(two())), 3);
      });

      it("Operations with 3 operands", function () {
        assert.equal(one(plus(seven(times(five())))), 36);
        assert.equal(three(times(four(plus(nine())))), 39);
        assert.equal(five(dividedBy(eight(minus(three())))), 1);
        assert.equal(seven(minus(six(dividedBy(two())))), 4);
      });
    });

    describe("Numbers returns its values", function () {
      var numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

      function makeTest(i) {
        it("Function '" + numbers[i] + "' returns: " + i, function () {
          assert.equal(window[numbers[i]](), i);
        });
      }

      for (var i = 0; i < 10; i++) {
        makeTest(i);
      }
    });
  });

  // Task #3
  // ============================================================================================
  describe("Task #3", function () {
    function add(a, b) {
      return a + b;
    };

    var add_ = defaultArguments(add, {
      b: 9
    });

    describe("Works with one, two or no parameters", function () {
      it("Looks good on first transformation", function () {
        assert.equal(add_(10), 19);
        assert.equal(add_(10, 7), 17);
        assert.isNaN(add_());
      });
      
      it("Looks good on first transformation", function(){
        add_ = defaultArguments(add_, {a:3});
        
        assert.equal(add_(), 12);
        assert.equal(add_(10), 19);
      });
      
      it("And after third transformation all still good", function(){
        add_ = defaultArguments(add_, {a:8, b: 19});
        
        assert.equal(add_(), 27);
        assert.equal(add_(10), 29);
      });
      
      it("Try to pass with non-used parameter", function(){
        add_ = defaultArguments(add_, {c: 146});
        
        assert.equal(add_(), 27);
        assert.equal(add_(10), 29);
      });
    });
  });

  // Task #4
  // ============================================================================================
  describe("Task #4", function () {

    describe("Find meeting time for one person", function () {
      it("Can find meeting time for person #1", function () {
        assert.isTrue(checkForMeetingAvailable('11:30', 60, [0]));
        assert.isTrue(checkForMeetingAvailable('17:30', 15, [0]));
      });

      it("Can find meeting time for person #2", function () {
        assert.isTrue(checkForMeetingAvailable('13:00', 30, [1]));
        assert.isTrue(checkForMeetingAvailable('18:00', 60, [1]));
      });

      it("Can find meeting time for person #3", function () {
        assert.isTrue(checkForMeetingAvailable('10:00', 60, [2]));
        assert.isTrue(checkForMeetingAvailable('17:00', 30, [2]));
      });
    });

    describe("Get NULL as result for unavailable meeting for one person", function () {
      it("Can find meeting time for person #1", function () {
        assert.isNull(checkForMeetingAvailable('11:30', 240, [0]));
      });

      it("Can find meeting time for person #2", function () {
        assert.isNull(checkForMeetingAvailable('08:00', 30, [1]));
      });

      it("Can find meeting time for person #3", function () {
        assert.isNull(checkForMeetingAvailable('09:00', 700, [2]));
      });
    });

    describe("Find meeting time for all businessmens", function () {
      it("Can find 1 hour meeting for 12:15", function () {
        assert.isTrue(checkForMeetingAvailable('12:15', 60, [0, 1, 2]));
      });

      it("Can find 15 min meeting for 17:30", function () {
        assert.isTrue(checkForMeetingAvailable('17:30', 15, [0, 1, 2]));
      });
    });

    describe("Get NULL as result for unavailable meeting for all businessmens", function () {
      it("Can find meeting time for person #1", function () {
        assert.isNull(checkForMeetingAvailable('11:30', 240, [0, 1, 2]));
      });

      it("Can find meeting time for person #2", function () {
        assert.isNull(checkForMeetingAvailable('08:00', 30, [0, 1, 2]));
      });

      it("Can find meeting time for person #3", function () {
        assert.isNull(checkForMeetingAvailable('09:00', 700, [0, 1, 2]));
      });
    });
  });
})();